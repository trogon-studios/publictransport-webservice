<?php

class Program{
	static function main(){
		$importers = self::getImporters();
		$processors = self::getProcessors();

		if(count($importers) > 0){
			foreach($importers as $key => $importer){
				$pkey = str_replace('Importer', 'Processor', $key);
				if(array_key_exists($pkey, $processors)){
					$data = $importer->load();
					$processors[$pkey]->process($data);
				}
			}
		}
	}

	static function getImporters(){
		$list = [];
		$files = self::getFiles('importer');
		foreach($files as $file){
			require_once($file);
			$name = basename($file, '.php');
			if(class_exists($name)){
				$class = new ReflectionClass($name);
				if($class->isInstantiable()){
					$list[$name] = $class->newInstance();
				}
			}
		}
		return $list;
	}

	static function getProcessors(){
		$list = [];
		$files = self::getFiles('processor');
		foreach($files as $file){
			require_once($file);
			$name = basename($file, '.php');
			if(class_exists($name)){
				$class = new ReflectionClass($name);
				if($class->isInstantiable()){
					$list[$name] = $class->newInstance();
				}
			}
		}
		return $list;
	}
	
	static function getFiles($path){
		$list = [];
		$files = scandir($path);
		foreach($files as $file){
			if(is_file($path . DIRECTORY_SEPARATOR . $file) && basename($file) != basename($file, '.php')){
				$list[] = $path . DIRECTORY_SEPARATOR . $file;
			}
		}
		return $list;
	}
}

Program::main();
