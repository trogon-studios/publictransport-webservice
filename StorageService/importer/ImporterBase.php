<?php require_once('models.php');

abstract class ImporterBase{
	protected $tmpPath;

	abstract function load();

	protected function clean(){
		if($this->tmpPath != null && strlen($this->tmpPath) > 0 && is_dir(dirname($this->tmpPath))){
			printf("[INFO]: Cleaning tmp GDA ZTM...\n");
			ImporterBase::delTree($this->tmpPath, true);
			printf("[INFO]: Cleaned tmp GDA ZTM...\n");
		}
	}

	public static function delTree($dir, $keep = false) { 
		$files = array_diff(scandir($dir), array('.', '..')); 
		foreach ($files as $file) {
			if(is_dir("$dir/$file"))
				self::delTree("$dir/$file");
			else
				unlink("$dir/$file"); 
		}
		if($keep)
			return true;
		else
			return rmdir($dir);
	}
}
