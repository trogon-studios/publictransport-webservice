<?php require_once('ImporterBase.php');

class PlGdaZtmImporter extends ImporterBase{
	const URL = 'http://www.ztm.gda.pl/rozklady/pobierz_rozklady.php';

	private $zipPath;

	public function __construct(){
		$this->tmpPath = realpath(__DIR__ . '/../_input/pl-gda-ztm');
		$this->zipPath = $this->tmpPath . '/data.zip';
	}

	public function load(){
		$this->clean();
		$this->download();
		$this->unzip();
		return $this->read();
	}

	private function download(){
		if(is_dir(dirname($this->zipPath))){
			$fd = fopen($this->zipPath, 'w');

			if($fd !== null){
				printf("[INFO]: Downloading GDA ZTM...\n");
				$ch = curl_init(self::URL);

				curl_setopt($ch, CURLOPT_TIMEOUT, 50);
				// write curl response to file
				curl_setopt($ch, CURLOPT_HEADER, false);
				curl_setopt($ch, CURLOPT_FILE, $fd); 
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

				curl_exec($ch);

				curl_close($ch);
				fclose($fd);
				printf("[INFO]: Downloaded GDA ZTM.\n");
			}
		}
	}

	private function unzip(){
		if(is_file($this->zipPath) && is_dir($this->tmpPath)){
			printf("[INFO]: Unzipping GDA ZTM...\n");
			$zip = new ZipArchive;
			$res = $zip->open($this->zipPath);
			if ($res === TRUE) {
				$zip->extractTo($this->tmpPath);
				$zip->close();
				printf("[INFO]: Unzipped GDA ZTM.\n");
				unlink($this->zipPath);
				return true;
			}
		}
		return false;
	}
	
	private function read(){
		printf("[INFO]: Importing GDA ZTM...\n");
		
		printf("[INFO]: Imported GDA ZTM.\n");
	}
}
